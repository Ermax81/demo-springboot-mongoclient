package com.example.demospringbootmongoclient;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
@AllArgsConstructor
public class Employee {

    @Id
    private String id;

    private String firstName;

    private String lastName;

    private String email;
}
