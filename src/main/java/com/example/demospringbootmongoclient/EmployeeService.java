package com.example.demospringbootmongoclient;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public List<Employee> getAll() {
        return employeeRepository.getAll();
    }

    public String save(Employee employee) {
        return employeeRepository.save(employee);
    }

    public String update(Employee employee) {
        return employeeRepository.update(employee);
    }

    public String delete(String id) {
        return employeeRepository.delete(id);
    }
}
