package com.example.demospringbootmongoclient;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class EmployeeRepository {

    private MongoClient client;

    private MongoDatabase database;

    private MongoClient getClient() {
        if ( client == null ) {
            client = new MongoClient("localhost", 27017);
        }
        return client;
    }

    private void getDatabase() {
        if ( client == null ) {
            client = getClient();
        }
        database = client.getDatabase("test");
    }

    private MongoCollection getCollection() {
        if ( client == null || database == null ) {
            getDatabase();
        }
        return database.getCollection("employee");
    }

    public List<Employee> getAll() {
        MongoCollection<Document> collection = getCollection();

        List<Employee> response = new ArrayList<>();

        for (Document d : collection.find()) {
            Employee e = new Employee(
                    d.get("_id").toString(),
                    d.getString("firstName"),
                    d.getString("lastName"),
                    d.getString("email")
            );
            response.add(e);
        }
        return response;
    }

    public String save(Employee emp) {
        MongoCollection<Document> collection = getCollection();

        Document employeeToSave = new Document();
        employeeToSave.append("firstName",emp.getFirstName());
        employeeToSave.append("lastName",emp.getLastName());
        employeeToSave.append("email",emp.getEmail());

        String response;
        try {
            collection.insertOne(employeeToSave);
            response = "Added successfully";
        } catch(Exception e) {
            response = "Issue when adding employee";
        }
        return response;
    }

    public String update(Employee emp) {
        MongoCollection<Document> collection = getCollection();

        Document employeeToSave = new Document();
        employeeToSave.append("firstName",emp.getFirstName());
        employeeToSave.append("lastName",emp.getLastName());
        employeeToSave.append("email",emp.getEmail());

        String response;
        try {
            BasicDBObject filter = new BasicDBObject("_id", new ObjectId(emp.getId()));
            collection.updateOne(filter, new BasicDBObject("$set",employeeToSave));
            response = "Modified successfully";
        } catch(IllegalArgumentException e) { // if emp.getId == null
            response = "Issue when modifying employee, id must be provided in json body";
        } catch(Exception e) {
            response = "Issue when modifying employee";
        }
        return response;
    }

    public String delete(String id) {
        MongoCollection<Document> collection = getCollection();

        String response;
        try {
            BasicDBObject filter = new BasicDBObject("_id", new ObjectId(id));
            collection.deleteOne(filter);
            response = "Deleted successfully";
        } catch(Exception e) {
            response = "Issue when deleting employee";
        }
        return response;
    }
}
