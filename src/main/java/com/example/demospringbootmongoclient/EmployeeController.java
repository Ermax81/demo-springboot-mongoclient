package com.example.demospringbootmongoclient;

import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/api")
public class EmployeeController {

    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    /**
     * Requete: curl -X GET -H "Accept: application/json" -H "Content-Type: application/json" http://localhost:8080/api/employee
     * Reponse: [{"id":"60ccb697744f053d8e6eab6d","firstName":"John","lastName":"Doe","email":"john.doe@test.com"}]
     */
    @GetMapping("/employee")
    public List<Employee> getAll() {
        return employeeService.getAll();
    }

    /**
     * Requete: curl -X POST -H "Accept: application/json" -H "Content-Type: application/json" -d '{"firstName":"Jane","lastName":"Doe","email":"jane.doe@test.com"}' http://localhost:8080/api/employee
     * Reponse: Added successfully
     * Requete: curl -X GET -H "Accept: application/json" -H "Content-Type: application/json" http://localhost:8080/api/employee
     * Reponse: [{"id":"60ccb697744f053d8e6eab6d","firstName":"John","lastName":"Doe","email":"john.doe@test.com"},{"id":"60cceaf9744f053d8e6eab70","firstName":"Jane","lastName":"Doe","email":"jane.doe@test.com"}]
     */
    @PostMapping("/employee")
    public String add(@RequestBody Employee employee) {
        return employeeService.save(employee);
    }

    /**
     * Requete: curl -X PUT -H "Accept: application/json" -H "Content-Type: application/json" -d '{"id":"60cceaf9744f053d8e6eab70","firstName":"Janet","lastName":"Doe","email":"janet.doe@test.com"}' http://localhost:8080/api/employee
     * Reponse: Modified successfully
     * Requete: curl -X GET -H "Accept: application/json" -H "Content-Type: application/json" http://localhost:8080/api/employee
     * Reponse: [{"id":"60ccb697744f053d8e6eab6d","firstName":"John","lastName":"Doe","email":"john.doe@test.com"},{"id":"60cceaf9744f053d8e6eab70","firstName":"Janet","lastName":"Doe","email":"janet.doe@test.com"}]
     */
    @PutMapping("/employee")
    public String update(@RequestBody Employee employee) {
        return employeeService.update(employee);
    }

    /**
     * Requete: curl -X DELETE http://localhost:8080/api/employee?id=60cceaf9744f053d8e6eab70
     * Reponse: Deleted successfully
     * Requete: curl -X GET -H "Accept: application/json" -H "Content-Type: application/json" http://localhost:8080/api/employee
     * Reponse: [{"id":"60ccb697744f053d8e6eab6d","firstName":"John","lastName":"Doe","email":"john.doe@test.com"}]
     */
    @DeleteMapping("/employee")
    public String delete(@PathParam("id") String id) {
        return employeeService.delete(id);
    }
}
