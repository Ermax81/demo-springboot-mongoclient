
### SpringBoot demo with MongoClient

Sources:
- [Spring Boot | Tutorial 21 : MongoClient and Embedded Mongo Part 1](https://youtu.be/222MwmHkjrw)
- [Spring Boot | Tutorial 22 : MongoClient and Embedded Mongo Part 2](https://youtu.be/WVjnaN86vME)
- [Spring Boot | Tutorial 23 : MongoClient and Embedded Mongo Part 3](https://youtu.be/I7KW_GaHmT4)


